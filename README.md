# jinogl
Nora's OpenGL engine in C++. The name of this project may change, and what it does will change as this turns into what I want it to.

## Build
I have exclusively tested this on NixOS. I use `nix shell.nix` to have the necessary packages to build this. You can use nix or find the packages yourself. Use the default `make` target to build this.
