with import <nixpkgs> { };
stdenv.mkDerivation {
  name = "jinogl";
  buildInputs = [ zlib glew glfw glm ];
}
