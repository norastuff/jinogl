# Define compiler and flags
CXX = g++
CXXFLAGS =

# Output executable name
EXEC = jinogl

# GLEW library path (assuming installed system-wide)
GLEW_LIB = -lGLEW

# GLFW library path (assuming installed system-wide)
GLFW_LIB = -lglfw


# GLM library path (assuming installed system-wide)
GLM_INC = -I/usr/include/glm

# OpenGL library
OPENGL_LIB = -lGL

# Default rule to build executable
$(EXEC): source/main.cpp $(wildcard source/*.cpp) | build common
	$(CXX) $(CXXFLAGS) $(GLM_INC) -I/usr/include -L/usr/lib $^ $(GLEW_LIB) $(GLFW_LIB) $(OPENGL_LIB) -o build/$(EXEC)
	cp -r common/* build/

# Create build directory if it doesn't exist
build:
	mkdir -p build

# Phony target for copying common files
common:
	mkdir -p build/common

# Clean rule to remove build directory and executable
clean:
	rm -rf build/ $(EXEC)

# Phony target to avoid conflicts with directory names
.PHONY: clean common

